import './App.css';
import { LoginForm } from "./components/LoginForm/LoginForm"
import { RegisterForm } from './components/RegisterForm/RegisterForm';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={LoginForm}/>
          <Route exact path="/register" component={RegisterForm}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
